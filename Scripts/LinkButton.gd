extends LinkButton

export(String) var scene_to_load

func _on_New_Game_pressed():
	Global.lives = 3
	get_tree().change_scene(str("res://Scenes/levels/" + scene_to_load + ".tscn"))


func _on_LinkButton2_pressed():
	get_tree().quit()
