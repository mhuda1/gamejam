extends Area2D



func _on_Key_body_entered(body):
	if "Player" in body.name:
		$AnimatedSprite.play("collected")
		call_deferred("disable")
		$pickup.play()
		body.add_keys(1)
		$Timer.start()

func disable():
	$CollisionShape2D.disabled = true
	

func _on_Timer_timeout():
	queue_free()
