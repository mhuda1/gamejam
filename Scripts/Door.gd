extends Area2D
export(String, FILE, "*.tscn,*.scn") var target_scene
export(bool) var closing_animation = false
export(int) var coins_needed = 0

var player_body = null

func _ready():
	player_body = null # to be sure
	# this only tmp objects which help set transformations
	Global.keys = 0
	Global.koin_temp = Global.koin
	Global.keys_needed = coins_needed
	
	if closing_animation:
		$ani.play("close")


func _on_Door_body_entered(body):
	player_body = body

func _on_Door_body_exited(body):
	player_body = null

func _process(delta):

	if Input.is_action_just_pressed('game_use'):
		if !target_scene:
			return
		
		if player_body:
#			if Global.key < coins_needed:
#				return
			if player_body.keys < coins_needed:
#				show_coins_alert((coins_needed - player_body.keys))
				$closed.play()
				return
			player_body.walk_in(get_node("."))

func open_ani():
	$ani.play("open")

func next_level():
	if !target_scene:
		return
	
	#Global.music_playback_time = get_tree().current_scene.Music.get_playback_position()
#	Global.save_playback_time_using_scene(get_tree().current_scene)
	
	var er = get_tree().change_scene(target_scene)
	if er != OK:
		print("Failed to change scene using Door")


